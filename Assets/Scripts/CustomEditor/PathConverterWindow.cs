#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PathConverterWindow : EditorWindow
{
	public string path = "InputPath...";
	public string convertedPath = "";

	[MenuItem("Window/Util/PathConverter")]
	private static void Init()
	{
		// 창 띄우기
		PathConverterWindow window = EditorWindow.GetWindow<PathConverterWindow>();
		// - PathConverterWindow 형식의 창을 단 하나만 생성하여 반환합니다.

		// 창 크기 설정
		window.minSize = new Vector2(20, 100);
	}

	private void OnGUI()
	{
		// 라벨
		GUILayout.Label("Original Path");

		// 경로 입력칸
		path = EditorGUILayout.TextField("Input Path...", path);

		// 버튼
		if (GUILayout.Button("Convert"))
		{
			// 경로를 컨버팅합니다.
			convertedPath = ConvertPath(path);
		}

		GUILayout.TextField(convertedPath);
	}

	private string ConvertPath(string path)
	{
		// Assets/Resources/Prefabs/MapSection/Section01.prefab
		// -> Prefabs/MapSection/Section01

		string resourcesString = "Resources/";

		int pathStartIndex = path.IndexOf(resourcesString) +
			(resourcesString.Length);

		int dotIndex = path.IndexOf('.');

		// 경로 변환
		string convertedPath = path.Substring(pathStartIndex, dotIndex - pathStartIndex);
		return convertedPath;
	}



}
#endif
