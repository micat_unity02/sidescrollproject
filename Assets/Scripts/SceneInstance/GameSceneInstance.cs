using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GameSceneInstance : SceneInstance
{
	private static MapSectionScriptableObject _MapSectionScriptableObject;

	[Header("# 씬에서 사용되는 카메라")]
	public FollowCamera m_UseFollowCamera;

	[Header("# 사용되는 UI")]
	public PlayerUI m_UsePlayerUI;

	[Header("# 가장 먼저 사용되는 Map Secion ID")]
	public int m_FirstMapSecionID;


	/// <summary>
	/// 현재 플레이어 캐릭터가 위치한 섹션을 나타냅니다.
	/// </summary>
	private MapSection _CurrentSection;

	/// <summary>
	/// 씬에 사용되는 카메라 영역들을 나타냅니다.
	/// </summary>
	private Dictionary<BoxCollider, CameraArea> _CameraAreas = new();

	/// <summary>
	/// 해당 씬에 사용되는 적 객체들을 나타냅니다.
	/// </summary>
	private Dictionary<Collider, EnemyCharacter> _EnemyCharacters = new();

	/// <summary>
	/// 로드된 맵 섹션 객체들을 나타냅니다.
	/// </summary>
	private Dictionary<int/*mapSecionID*/, MapSection> _LoadedMapSections = new();

	protected override void Awake()
	{
		base.Awake();

		if (_MapSectionScriptableObject == null)
		{
			_MapSectionScriptableObject = Resources.Load<MapSectionScriptableObject>(
				"ScriptableObject/MapSectionData");
		}
	}

	private void Start()
	{
		// Create First Map Secion
		CreateFirstMapSection();
	}

	/// <summary>
	/// 첫 번째 맵 섹션을 생성합니다.
	/// </summary>
	private void CreateFirstMapSection()
	{
		// 로드시킬 첫 번째 맵 섹션 프리팹 경로를 얻습니다.
		string firstMapSectionPath = 
			_MapSectionScriptableObject.GetMapSectionData(m_FirstMapSecionID).mapSectionPath;

		// 경로를 기반으로 프리팹을 로드하여 복사 생성합니다.
		MapSection firstMapSection = Instantiate(Resources.Load<MapSection>(firstMapSectionPath));

		// 로드된 맵 섹션을 등록합니다.
		_LoadedMapSections.Add(m_FirstMapSecionID, firstMapSection);
	}

	/// <summary>
	/// 사용되지 않는 맵 섹션을 제거합니다.
	/// </summary>
	private void ClearMapSection()
	{
		// 제거된 맵 섹션 ID 를 나타냅니다.
		List<int> destroyedMapSectionID = new();

		// 현재 로드된 맵 섹션 중, 더이상 사용되지 않는 섹션을 제거합니다.
		foreach(KeyValuePair<int, MapSection> loadedMapSection in _LoadedMapSections)
		{
			// 로드된 맵 섹션 ID 하나를 얻습니다.
			int sectionID = loadedMapSection.Key;

			// 로드된 맵 섹션 객체를 얻습니다.
			MapSection mapSection = loadedMapSection.Value;

			// 현재 맵 섹션에서 사용되는 섹션인 경우
			if (_CurrentSection.m_AdjacentSectionID.Contains(sectionID))
			{
				// 이 섹션은 제거에서 제외됩니다.
				continue;
			}

			// 섹션 제거
			Destroy(mapSection.gameObject);
			destroyedMapSectionID.Add(sectionID);

		}

		foreach(int id in destroyedMapSectionID)
		{
			_LoadedMapSections.Remove(id);
		}
	}

	/// <summary>
	/// 맵 섹션을 로드합니다.
	/// </summary>
	private void LoadMapSection()
	{
		// 현재 섹션에서 사용되는 맵 섹션을 로드합니다.
		foreach (int adjacentSectionID in _CurrentSection.m_AdjacentSectionID)
		{ 
			// 이미 로드된 섹션이 존재한다면
			if (_LoadedMapSections.ContainsKey(adjacentSectionID))
			{
				// 로드에서 제외합니다.
				continue;
			}

			// 로드되어야 할 맵 섹션을 얻습니다.
			MapSectionDataElem mapSectionData = _MapSectionScriptableObject.
				GetMapSectionData(adjacentSectionID);

			// 비동기 로드를 진행합니다.
			ResourceRequest asyncRequest = Resources.LoadAsync<MapSection>(
				mapSectionData.mapSectionPath);

			asyncRequest.completed += (AsyncOperation ao) =>
			{
				MapSection mapSection = Instantiate((ao as ResourceRequest).asset as MapSection);

				_LoadedMapSections.Add(mapSectionData.mapSectionID, mapSection);
			};
		}
	}

	/// <summary>
	/// BoxCollider 와 일치하는 카메라 영역을 얻습니다.
	/// </summary>
	/// <param name="key">CameraArea 컴포넌트와 함께 사용되는 BoxCollider 를 전달합니다.</param>
	/// <returns></returns>
	public CameraArea GetCameraArea(BoxCollider key)
	{
		if (_CameraAreas.TryGetValue(key, out CameraArea area))
		{
			return area;
		}

		return null;
	}

	/// <summary>
	/// 카메라 영역을 등록합니다.
	/// </summary>
	/// <param name="cameraArea">등록시킬 카메라 영역을 전달합니다.</param>
	public void RegisterCameraArea(CameraArea cameraArea)
	{
		_CameraAreas.Add(cameraArea.area, cameraArea);
	}

	/// <summary>
	/// 적 객체를 등록합니다.
	/// </summary>
	/// <param name="newEnemyCharacter">등록시킬 적 객체를 전달합니다.</param>
	public void RegisterEnemyCharacter(EnemyCharacter newEnemyCharacter)
	{
		_EnemyCharacters.Add(newEnemyCharacter.enemyCollider, newEnemyCharacter);
	}

	public EnemyCharacter GetEnemyCharacter(Collider enemyCollider)
	{
		if (_EnemyCharacters.TryGetValue(enemyCollider, out EnemyCharacter enemyCharacter))
		{
			return enemyCharacter;
		}
		else return null;
	}

	/// <summary>
	/// 플레이어 캐릭터가 위치한 섹션을 갱신합니다.
	/// </summary>
	/// <param name="newSection"></param>
	public void UpdateSection(MapSection newSection)
	{
		// 같은 섹션인 경우 무시
		if (_CurrentSection == newSection) return;

		_CurrentSection = newSection;

		// 사용되지 않는 맵 섹션을 제거합니다.
		ClearMapSection();

		// 사용되는 맵 섹션을 로드합니다.
		LoadMapSection();
	}

}

