using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
	[Header("# HUD 패널")]
	public RectTransform m_HUDPanel;

	[Header("# Player Hp 패널")]
	public PlayerHpPanel m_PlayerHpPanel;


	/// <summary>
	/// RectTransform 에 대한 프로퍼티입니다.
	/// </summary>
	public RectTransform rectTransform => transform as RectTransform;

	/// <summary>
	/// HUD 객체를 생성합니다.
	/// </summary>
	/// <typeparam name="THUD">생성시킬 HUD 객체 형식을 전달합니다.</typeparam>
	/// <param name="hudPrefab">생성시킬 HUD 객체 프리팹을 전달합니다.</param>
	/// <returns></returns>
	public THUD CreateHUD<THUD>(THUD hudPrefab)
		where THUD : HUDBase
	{
		// HUD 객체 생성
		THUD hudInstance = Instantiate(hudPrefab, m_HUDPanel);

		// HUD 객체 초기화
		hudInstance.OnHUDInitialized();

		// 생성된 HUD 객체 반환
		return hudInstance;
	}



}
