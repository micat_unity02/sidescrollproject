using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 플레이어 HP 를 나타내기 위한 패널에 사용되는 컴포넌트입니다.
/// </summary>
public sealed class PlayerHpPanel : MonoBehaviour
{
	[Header("# Hpbar")]
	public Image m_HpValueImage;

	/// <summary>
	/// 최대 체력입니다.
	/// </summary>
	private float _MaxHp;

	/// <summary>
	/// Hp 패널 초기화
	/// </summary>
	/// <param name="maxHp">최대 체력을 전달합니다.</param>
	public void InitializeHpPanel(float maxHp)
	{
		_MaxHp = maxHp;
		SetHpValue(_MaxHp);
	}

	/// <summary>
	/// Hp 수치를 조절합니다.
	/// </summary>
	/// <param name="newHpValue">설정시킬 체력을 전달합니다.</param>
	public void SetHpValue(float newHpValue)
	{
		float fill = newHpValue / _MaxHp;
		m_HpValueImage.fillAmount = fill;
	}



}
