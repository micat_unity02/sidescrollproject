﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHUD : HUDBase
{
	[Header("# 적 이름")]
	public TMP_Text m_NameText;

	[Header("# 적 체력")]
	public Image m_HPbar;

	/// <summary>
	/// 최대 체력입니다.
	/// </summary>
	private float _MaxHp;

	public void SetEnemyInfo(string enemyName, float maxHp)
	{
		m_NameText.text = enemyName;
		_MaxHp = maxHp;
	}

	public void SetHp(float hp)
	{
		m_HPbar.fillAmount = hp / _MaxHp;
	}



}