using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
	[Header("# 이동 속력")]	
	public float m_MoveSpeed;

	/// <summary>
	/// 목표 위치입니다.
	/// </summary>
	private Vector3 _TargetPosition;

	/// <summary>
	/// 속도입니다.
	/// </summary>
	private Vector3 _Velocity;

	/// <summary>
	/// 오른쪽 방향임을 나타냅니다.
	/// </summary>
	private bool _IsRight;

	/// <summary>
	/// 이동 끝을 나타내는 프로퍼티입니다.
	/// </summary>
	public bool isMoveFinished => (transform.position == _TargetPosition);

	private BehaviorController _BehaviorController;

	private EnemyCharacter _EnemyCharacter;

	public bool isRight => _IsRight;
	

	public Vector3 velocity => _Velocity;

	private void Awake()
	{
		_EnemyCharacter = GetComponent<EnemyCharacter>();
		_BehaviorController = GetComponent<BehaviorController>();
	}

	private void FixedUpdate()
	{
		// 목표 위치를 갱신합니다.
		UpdateTargetPosition();

		// 이동시킵니다.
		Move();
	}

	/// <summary>
	/// 목표 위치를 갱신합니다.
	/// </summary>
	private void UpdateTargetPosition()
	{
		// 목표 위치를 얻습니다.
		Vector3 targetPosition = _BehaviorController.GetKeyAsValue<Vector3>(
			BehaviorController_Enemy.KEYNAME_MOVETARGET);

		// 이동 블록 여부 확인
		bool isMoveBlocked = _BehaviorController.GetKeyAsValue<bool>(
			BehaviorController_Enemy01.KEYNAME_BLOCKMOVEMENT);
        if (isMoveBlocked) targetPosition = transform.position;

        targetPosition.y = transform.position.y;

		// 목표 위치 설정
		_TargetPosition = targetPosition;
	}

	private void Move()
	{
		CalculateVelocityX();

		UpdateDirection();

		transform.position += _Velocity;
	}

	/// <summary>
	/// X 축 속도를 계산합니다.
	/// </summary>
	private void CalculateVelocityX()
	{
		// 오브젝트의 위치를 얻습니다.
		Vector3 currentPosition = transform.position;

		// 남은 거리
		float remainDestance = Mathf.Abs(_TargetPosition.x - currentPosition.x);

		// X 축 속도를 설정합니다.
		float velocityX = m_MoveSpeed * Time.fixedDeltaTime;
		velocityX = (velocityX > remainDestance) ? remainDestance : velocityX;
		velocityX *= (currentPosition.x < _TargetPosition.x) ? 1 : -1;

		_Velocity.x = velocityX;
	}

	/// <summary>
	/// 방향을 갱신합니다.
	/// </summary>
	private void UpdateDirection()
	{
		// 이동중인 경우에만 방향이 갱신되도록 합니다.
		if (Mathf.Abs(_Velocity.x) > 0.0f)
		{
			_IsRight = _Velocity.x > 0.0f;
			_EnemyCharacter.SetFlipX(_IsRight);
		}
	}


	public void SetTargetPosition(Vector3 targetPosition)
	{
	}

}
