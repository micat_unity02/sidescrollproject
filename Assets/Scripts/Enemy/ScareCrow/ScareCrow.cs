﻿using UnityEngine;

public class ScareCrow : EnemyCharacter
{
	private const string ANIMPARAMNAME_ONHIT = "_OnHit";

	protected override void OnDamaged(PlayerCharacter playerCharacter, float calculatedDamage)
	{
		base.OnDamaged(playerCharacter, calculatedDamage);

		animControler.SetTrigger(ANIMPARAMNAME_ONHIT);
	}
}
