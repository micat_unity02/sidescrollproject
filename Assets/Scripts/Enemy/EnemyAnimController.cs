using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimController : MonoBehaviour
{
	private Animator _Animator;
	public Animator animator => _Animator ?? (_Animator = GetComponentInChildren<Animator>());

	public void SetBool(string paramName, bool newValue)
		=> animator.SetBool(paramName, newValue);
	public void SetFloat(string paramName, float newValue)
		=> animator.SetFloat(paramName, newValue);
	public void SetInt(string paramName, int newValue)
		=> animator.SetInteger(paramName, newValue);
	public void SetTrigger(string paramName)
		=> animator.SetTrigger(paramName);
}
