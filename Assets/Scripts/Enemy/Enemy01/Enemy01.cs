using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Enemy01 : EnemyCharacter
{

	// 살아있는 상태 / 사망 상태
	// 살아있는 상태
	// ㄴ대기, 공격
	//   ㄴ대기 상태 : 랜덤한 위치로 이동 -> 대기
	//   ㄴ공격 상태 : 플레이어를 추적하며 공격
	// 사망 상태
	// ㄴ계속 대기

	public const string ANIMPARAM_SPEED = "_Speed";
	public const string ANIMPARAM_ATTACK = "_Attack";

	private Enemy01Attack _AttackComponent;


	public Enemy01Attack attackComponent => _AttackComponent ?? (_AttackComponent = GetComponent<Enemy01Attack>());


	protected override void Update()
	{
		base.Update();

		UpdateAnimationParam();
	}

	private void UpdateAnimationParam()
	{
		animControler.SetFloat(ANIMPARAM_SPEED, 
			movement.velocity.sqrMagnitude > 0.0f ? 1.0f : 0.0f);
	}

	protected override void OnDamaged(
		PlayerCharacter playerCharacter,
		float calculatedDamage)
	{
		base.OnDamaged(playerCharacter, calculatedDamage);

		BehaviorController_Enemy01 bhController = behaviorController as BehaviorController_Enemy01;

		bhController.StopBehavior();
		bhController.StartEnemy01Behavior();

		(animControler as Enemy01AnimController).OnDamaged();
	}

	protected override void OnDead()
	{
		base.OnDead();
		(animControler as Enemy01AnimController).OnDead();
	}

	/// <summary>
	/// 적 객체 제거
	/// </summary>
	public void DestroyEnemy()
	{
		Destroy(gameObject);
	}




}
