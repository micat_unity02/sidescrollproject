using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy01AnimController : EnemyAnimController
{
	[Header("# Material Parameters")]
	public bool m_Damaged;
	public float m_Alpha = 1.0f;

	private Enemy01 _Enemy;

	private Material _Material;

	private void Awake()
	{
		_Enemy = GetComponentInParent<Enemy01>();

		// 적 객체들이 다른 메터리얼을 가질 수 있도록 메터리얼 객체들을 복사시킵니다.
		_Material = Instantiate(_Enemy.spriteRenderer.material);
		_Material.name = "Enemy01_DynamicMaterial";
		_Enemy.spriteRenderer.material = _Material;
	}

	private void Update()
	{
		UpdateMaterialParams();
	}

	private void UpdateMaterialParams()
	{
		_Material.SetInt("_IsDamaged", m_Damaged ? 1 : 0);
		_Material.SetFloat("_Alpha", m_Alpha);
	}

	/// <summary>
	/// 공격 영역을 확인합니다.
	/// </summary>
	private void AnimEvent_CheckAttackArea()
	{
		_Enemy.attackComponent.CheckAttackArea();
	}

	/// <summary>
	/// 공격 애니메이션이 끝났을 경우 호출됩니다.
	/// </summary>
	private void AnimEvent_OnAttackFinished()
	{
		_Enemy.attackComponent.OnAttackFinished();
	}

	public void OnDamaged()
	{
		SetTrigger("_Damaged");
	}

	public void OnDead()
	{
		SetTrigger("_Dead");
	}

	public void AnimEvent_Destroy()
	{
		_Enemy.DestroyEnemy();
	}

}
