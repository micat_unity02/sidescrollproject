using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy01Attack : MonoBehaviour
{
	[Header("# 공격 영역 관련")]
	public Vector3 m_AttackAreaSize;

	private Enemy01 _EnemyCharacter;

	#region DEBUG
	DrawGizmoCubeInfo _DrawGizmoAttackInfo;
	#endregion

	private void Awake()
	{
		_EnemyCharacter = GetComponent<Enemy01>();
	}

	public void Attack()
	{

		// 공격 애니메이션 재생
		_EnemyCharacter.animControler.SetTrigger(
			Enemy01.ANIMPARAM_ATTACK);
	}

	/// <summary>
	/// 공격 영역을 검사합니다.
	/// </summary>
	public void CheckAttackArea()
	{
		// 적 캐릭터 충돌박스 크기
		Vector3 enemyBodySize = (_EnemyCharacter.enemyCollider as BoxCollider).size;

		// 오프셋 X 계산
		float offesetX = (_EnemyCharacter.movement.isRight ? 1 : -1) *
			((enemyBodySize.x + m_AttackAreaSize.x) * 0.5f);
		Vector3 offset = Vector3.right * offesetX;

		Collider[] detectedCollisions = PhysicsExt.OverlapBox(
			out _DrawGizmoAttackInfo,
			transform.position + offset,
			m_AttackAreaSize * 0.5f,
			Quaternion.identity,
			1 << LayerMask.NameToLayer(Constants.LAYERNAME_PLAYER));

		if (detectedCollisions.Length > 0)
		{
			// Get SceneInstance
			GameSceneInstance sceneInst = SceneManager.instance.GetSceneInstance<GameSceneInstance>();

			// 적 정보를 얻습니다.
			ref EnemyData enemyData = ref _EnemyCharacter.enemyData;

			// 플레이어 캐릭터에게 피해를 입힙니다.
			sceneInst.playerController.controlledCharacter.OnDamaged(enemyData.atk);
		}
	}

	public void OnAttackFinished()
	{
		// 이동을 잠시 멈춥니다.
		_EnemyCharacter.behaviorController.SetKey(
			BehaviorController_Enemy01.KEYNAME_BLOCKMOVEMENT, false);
	}

#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		PhysicsExt.DrawGizmoOverlapBox(in _DrawGizmoAttackInfo);
	}
#endif


}
