﻿using UnityEngine;


[RequireComponent(typeof(Collider))]
public class EnemyCharacter : MonoBehaviour
{
	/// <summary>
	/// 적 HUD 프리팹입니다.
	/// </summary>
	protected static EnemyHUD m_EnemyHUDPrefab;

	/// <summary>
	/// HUD 가 표시될 때 적용될 오프셋
	/// </summary>
	public Vector2 m_HUDOffset;

	/// <summary>
	/// 적 정보를 모두 가지는 ScriptableObject 에셋
	/// </summary>
	private static EnemyDataScriptableObject _EnemyDataScriptableObject;

	[Header("# 적 코드")]
	public string m_EnemyCode;

	[Header("# 방향 변경 시 SpriteRenderer 에 적용될 오프셋")]
	public Vector3 m_SpriteDirectionOffset;


	/// <summary>
	/// 마지막으로 공격당한 시간을 나타냅니다.
	/// </summary>
	private float _LastDamagedTime;

	private Collider _EnemyCollider;

	private BehaviorController _BehaviorController;

	private EnemyAnimController _AnimController;

	private EnemyMovement _Movement;

	private SpriteRenderer _SpriteRenderer;

	/// <summary>
	/// 적 정보입니다.
	/// </summary>
	private EnemyData _EnemyData;


	/// <summary>
	/// 적 체력입니다.
	/// </summary>
	protected float _CurrentHp;

	public Collider enemyCollider => _EnemyCollider ?? 
		(_EnemyCollider = GetComponent<Collider>());

	public EnemyAnimController animControler => _AnimController ??
		(_AnimController = GetComponentInChildren<EnemyAnimController>());

	public EnemyMovement movement => _Movement ?? (_Movement = GetComponent<EnemyMovement>());

	public BehaviorController behaviorController => _BehaviorController ?? (
		_BehaviorController = GetComponent<BehaviorController>());
	public SpriteRenderer spriteRenderer => _SpriteRenderer ?? 
		(_SpriteRenderer = GetComponentInChildren<SpriteRenderer>());

	/// <summary>
	/// 사용되는 적 HUD 객체를 나타내는 프로퍼티입니다.
	/// </summary>
	protected EnemyHUD enemyHUD { get; private set; }

	public ref EnemyData enemyData => ref _EnemyData;



	protected virtual void Awake()
	{
		// 적 정보 에셋을 로드합니다.
		if (_EnemyDataScriptableObject == null)
		{
			_EnemyDataScriptableObject = Resources.Load<EnemyDataScriptableObject>("ScriptableObject/EnemyData");
		}

		// 적 HUD 에셋을 로드합니다.
		if (m_EnemyHUDPrefab == null)
		{
			m_EnemyHUDPrefab = Resources.Load<EnemyHUD>("Prefabs/UI/HUD/HUD_Enemy");
		}
		

		// 적 코드에 해당하는 적 정보를 얻습니다.
		_EnemyData = _EnemyDataScriptableObject.GetEnemyData(m_EnemyCode);
		_CurrentHp = _EnemyData.maxHP;

		// 씬에 적 객체 등록
		GameSceneInstance sceneInstance = SceneManager.instance.GetSceneInstance<GameSceneInstance>();
		sceneInstance.RegisterEnemyCharacter(this);

		// 적 레이어 설정
		gameObject.layer = LayerMask.NameToLayer(Constants.LAYERNAME_ENEMY);
	}

	protected virtual void Update()
	{
		if (enemyHUD != null) OnHUDTick();
	}



	/// <summary>
	/// 대미지를 적용합니다.
	/// </summary>
	/// <param name="damage"></param>
	public void ApplyDamage(PlayerCharacter playerCharacter, float damage)
	{
		OnDamaged(playerCharacter, CalculateDamage(damage));
	}

	/// <summary>
	/// 대미지를 계산합니다.
	/// </summary>
	/// <param name="damage"></param>
	/// <returns></returns>
	protected float CalculateDamage(float damage)
	{
		damage -= _EnemyData.def;
		if (damage < 0.0f) damage = 0.0f;
		return damage;
	}

	/// <summary>
	/// 대미지를 입었을 경우 호출되는 메서드입니다.
	/// </summary>
	/// <param name="playerCharacter">피해를 가한 플레이어 캐릭터 객체가 전달됩니다.</param>
	/// <param name="calculatedDamage">계산된 피해량이 전달됩니다.</param>
	/// <returns></returns>
	protected virtual void OnDamaged(
		PlayerCharacter playerCharacter, 
		float calculatedDamage)
	{
		// 체력을 깎습니다.
		_CurrentHp -= calculatedDamage;

		// 마지막으로 공격당한 시간을 기록합니다.
		_LastDamagedTime = Time.time;

		behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_ISAGGRESSIVESTATE, true);
		behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_LASTDAMAGEDTIME, _LastDamagedTime);
		behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER, playerCharacter);

		// HUD 객체 생성
		if (enemyHUD == null) OnHUDCreated();

		// 적 사망
		if (_CurrentHp <= 0.0f)
		{
			OnDead();
		}
	}

	/// <summary>
	/// HUD 객체가 생성될 경우 호출됩니다.
	/// </summary>
	protected virtual void OnHUDCreated()
	{
		// Get SceneInstance
		GameSceneInstance sceneInstance = 
			SceneManager.instance.GetSceneInstance<GameSceneInstance>();

		// Get PlayerUI
		PlayerUI playerUI = sceneInstance.m_UsePlayerUI;

		// 적 HUD 생성
		enemyHUD = playerUI.CreateHUD(m_EnemyHUDPrefab);

		// HUD에 적 정보 설정
		enemyHUD.SetEnemyInfo(_EnemyData.enemyName, _EnemyData.maxHP);
		enemyHUD.SetHp(_CurrentHp);
	}

	/// <summary>
	/// HUD 객체가 생성된 이후 매 프레임마다 호출됩니다.
	/// </summary>
	protected virtual void OnHUDTick()
	{
		// HUD 객체 제거
		if (Time.time >= _LastDamagedTime + Constants.ENEMYHUD_LIFETIMESECONDS)
		{
			OnHUDDestroyed();
			return;
		}

		// HUD 위치 설정
		Vector3 hudWorldPosition = transform.position;
		hudWorldPosition.x += m_HUDOffset.x;
		hudWorldPosition.y += m_HUDOffset.y;

		enemyHUD.SetWorldPosition(hudWorldPosition);

		// 현재 체력 갱신
		enemyHUD.SetHp(_CurrentHp);
	}

	/// <summary>
	/// HUD 객체가 제거될 때 호출됩니다.
	/// </summary>
	protected virtual void OnHUDDestroyed()
	{
		// 적 HUD 객체 제거
		Destroy(enemyHUD.gameObject);
		enemyHUD = null;
	}

	/// <summary>
	/// 적 사망 시 호출됩니다.
	/// </summary>
	protected virtual void OnDead()
	{
		// 적 HUD 제거
		if (enemyHUD != null) OnHUDDestroyed();

		// 사망 상태로 설정합니다.
		behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_ISDEAD, true);
	}

	public void SetFlipX(bool isRight)
	{
		// 이전에 적용된 값
		bool prevFlipX = spriteRenderer.flipX;

		// 새로 설정될 값
		bool newFlipX = !isRight;

		// 새로운 값이 설정되는 경우
		if (prevFlipX != newFlipX)
		{
			// SpriteRenderer 의 그려지는 방향을 변경합니다,.
			spriteRenderer.flipX = newFlipX;

			// 오프셋을 적용합니다.
			Vector3 offset = m_SpriteDirectionOffset;
			offset.x *= (isRight ? 1 : -1);

			spriteRenderer.transform.localPosition = offset;
		}
	}
}
