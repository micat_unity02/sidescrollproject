using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterAttack : MonoBehaviour
{
	[Header("# 공격 영역 관련")]
	/// <summary>
	/// 공격 영역 크기
	/// </summary>
	public Vector3 m_AttackAreaSize;

	/// <summary>
	/// 공격 영역 오프셋 (오른쪽을 기본으로 합니다.)
	/// </summary>
	public Vector3 m_AttackAreaOffset;

	/// <summary>
	/// 적 레이어
	/// </summary>
	public LayerMask m_EnemyLayerMask;


	/// <summary>
	/// 목표 콤보 카운트
	/// </summary>
	private int _TargetComboCount;

	/// <summary>
	/// 현재 진행중인 콤보 카운트
	/// </summary>
	private int _ComboCount;

	public int comboCount => _ComboCount;

	/// <summary>
	/// 다음 공격 입력 체크중임을 나타냅니다.
	/// </summary>
	private bool _NextAttackInputCheck;

	/// <summary>
	/// 현재 공격중임을 나타냅니다.
	/// </summary>
	private bool _IsAttack;

	/// <summary>
	/// 공격 영역이 활성화 되었음을 나타냅니다.
	/// </summary>
	private bool _AttackAreaIsEnabled;

	/// <summary>
	/// 오른쪽 방향을 보고 있음을 나타냅니다.
	/// </summary>
	private bool _IsRight;

	/// <summary>
	/// 공격시에 감지된 적 객체를 잠시 저장해둘 리스트
	/// </summary>
	private List<EnemyCharacter> _DetectedEnemyCharacter = new();

	/// <summary>
	/// 플레이어 캐릭터 객체를 나타냅니다.
	/// </summary>
	private PlayerCharacter _PlayerCharacter;

	/// <summary>
	/// 공격 시작, 끝 이벤트
	/// </summary>
	public event System.Action onAttackStarted;
	public event System.Action onAttackFinished;

	#region DEBUG
	private DrawGizmoCubeInfo _AttackAreDrawInfo;
	#endregion

	private void Awake()
	{
		_PlayerCharacter = GetComponent<PlayerCharacter>();
	}

	private void Update()
	{
		if (_AttackAreaIsEnabled)
		{
			ApplyDamage(CheckAttackArea());
		}
	}

	/// <summary>
	/// 공격 영역을 확인합니다.
	/// </summary>
	private Collider[] CheckAttackArea()
	{
		Vector3 offset = m_AttackAreaOffset;
		offset.x *= (_IsRight) ? 1 : -1;
		Vector3 center = transform.position;

		Collider[] detectedColliders = PhysicsExt.OverlapBox(
			out _AttackAreDrawInfo,
			center + offset,
			m_AttackAreaSize * 0.5f,
			Quaternion.identity,
			m_EnemyLayerMask);

		return detectedColliders;
	}

	private void ApplyDamage(Collider[] enemyColliders)
	{
		// 처리할 객체가 존재하지 않는 경우 함수 호출 종료.
		if (enemyColliders == null) return;
		if (enemyColliders.Length == 0) return;

		// Get SceneInstance
		GameSceneInstance sceneInstance = 
			SceneManager.instance.GetSceneInstance<GameSceneInstance>();




		foreach(Collider enemyCollider in enemyColliders)
		{
			EnemyCharacter enemyCharacter = sceneInstance.GetEnemyCharacter(enemyCollider);

			// 감지되지 않은 적 객체인지 확인합니다.
			if (!_DetectedEnemyCharacter.Contains(enemyCharacter))
			{
				// 리스트에 추가합니다.
				_DetectedEnemyCharacter.Add(enemyCharacter);

				// 대미지 처리를 진행합니다.
				enemyCharacter.ApplyDamage(_PlayerCharacter, 100);
			}
		}
	}



	public void OnAttackKeyPressed()
	{
		// 공격이 진행중이라면
		if (_IsAttack)
		{
			// 공격 입력이 가능한 상태라면
			if (_NextAttackInputCheck) ++_TargetComboCount;

			_TargetComboCount = Mathf.Clamp(_TargetComboCount, 0, 3);
		}
		// 공격이 진행중이 아니라면
		else
		{
			++_TargetComboCount;
			++_ComboCount;
		}
	}

	/// <summary>
	/// 다음 공격 입력 체크 시작 시 호출됩니다.
	/// </summary>
	public void OnNextAttackCheckStarted()
	{
		_NextAttackInputCheck = true;
	}

	/// <summary>
	/// 다음 공격 입력 체크가 끝나는 경우 호출됩니다.
	/// </summary>
	public void OnNextAttackCheckFinished()
	{
		_NextAttackInputCheck = false;
	}

	public void OnAttackStarted()
	{
		_IsAttack = true;
		onAttackStarted?.Invoke();
	}
	/// <summary>
	/// 공격 한 번이 끝날 때마다 호출됩니다.
	/// </summary>
	public void OnAttackFinished()
	{
		// 감지된 적 객체들을 모두 비웁니다.
		_DetectedEnemyCharacter.Clear();

		// 다음 공격이 존재하지 않는 경우
		if (_TargetComboCount == _ComboCount)
		{
			_ComboCount = 0;
			_TargetComboCount = 0;
			_IsAttack = false;
			onAttackFinished?.Invoke();
		}
		// 다음 공격이 존재하는 경우
		else ++_ComboCount;
	}

	/// <summary>
	/// 방향을 갱신합니다.
	/// </summary>
	/// <param name="isRight"></param>
	public void UpdateDirection(bool isRight)
	{
		_IsRight = isRight;
	}

	public void EnableAttackArea() => _AttackAreaIsEnabled = true;

	public void DisableAttackArea() => _AttackAreaIsEnabled = false;



#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		if (_AttackAreaIsEnabled)
		{
			PhysicsExt.DrawGizmoOverlapBox(in _AttackAreDrawInfo);
		}
	}
#endif
}
