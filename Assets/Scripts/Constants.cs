﻿using UnityEngine;
public static class Constants
{

	public const string ANIMPARAM_ISMOVE = "_IsMove";
	public const string ANIMPARAM_ISGROUNDED = "_IsGrounded";
	public const string ANIMPARAM_COMBOCOUNT = "_ComboCount";

	/// <summary>
	/// 플레이어 캐릭터 레이어 이름
	/// </summary>
	public const string LAYERNAME_PLAYER = "PlayerCharacter";

	/// <summary>
	/// 적 캐릭터 레이어 이름
	/// </summary>
	public const string LAYERNAME_ENEMY = "EnemyCharacter";

	/// <summary>
	/// 적 HUD 가 표시되는 시간
	/// </summary>
	public const float ENEMYHUD_LIFETIMESECONDS = 20.0f;

	public static Vector2 SCREENSIZE => new Vector2(1600.0f, 900.0f);
}
