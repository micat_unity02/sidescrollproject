using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSection : MonoBehaviour
{
	[Header("# 섹션 영역")]
	public Transform m_MinPosition;
	public Transform m_MaxPosition;

	[Header("# 이 섹션과 함께 사용될 섹션 ID")]
	public List<int> m_AdjacentSectionID = new();


	/// <summary>
	/// 씬 객체를 나타냅니다.
	/// </summary>
	private GameSceneInstance _SceneInstance;

	private void Start()
	{
		_SceneInstance = SceneManager.instance.GetSceneInstance<GameSceneInstance>();
	}

	private void Update()
	{
		// 플레이어 캐릭터가 섹션 내에 있는지 확인합니다.
		bool isIn = CheckPlayerCharacterPosition();

		// 플레이어 캐릭터가 이 섹션 내에 위치한 경우 섹션 갱신
		if (isIn) _SceneInstance.UpdateSection(this);
	}

	/// <summary>
	/// 플레이어 캐릭터 위치를 확인합니다.
	/// </summary>
	private bool CheckPlayerCharacterPosition()
	{
		PlayerCharacter playerCharacter = _SceneInstance.playerController.controlledCharacter;
		if (playerCharacter == null) return false;

		// 플레이어 캐릭터 위치를 얻습니다.
		Vector3 currentPosition = playerCharacter.transform.position;

		// 영역의 끝점입니다.
		Vector3 areaMin = m_MinPosition.position;
		Vector3 areaMax = m_MaxPosition.position;

		if (areaMin.x > currentPosition.x) return false;
		else if (areaMax.x < currentPosition.x) return false;
		return true;
	}


#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.white;

		Vector3 size = (m_MaxPosition.position - m_MinPosition.position);
		Vector3 sectionCenter = m_MinPosition.position + (size * 0.5f);
		Gizmos.DrawWireCube(sectionCenter, size);
	}
#endif


}
