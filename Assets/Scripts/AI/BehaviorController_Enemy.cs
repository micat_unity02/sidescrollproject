using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorController_Enemy : BehaviorController
{
	/// <summary>
	/// 적이 공격을 받아 공격적인 상태가 되었음을 나타내기 위한 키입니다.
	/// </summary>
	public const string KEYNAME_ISAGGRESSIVESTATE = "IsAggressiveState";
	/// <summary>
	/// 마지막으로 공격을 받은 시간을 나타내기 위한 키입니다.
	/// </summary>
	public const string KEYNAME_LASTDAMAGEDTIME = "LastDamagedTime";
	/// <summary>
	/// 공격적인 상태가 해제되기까지 걸리는 시간을 나타내기 위한 키입니다.
	/// </summary>
	public const string KEYNAME_TIMETOCALMDOWN = "TimeToCalmDown";
	/// <summary>
	/// 이동 목표 위치를 나타내는 키입니다.
	/// </summary>
	public const string KEYNAME_MOVETARGET = "MoveTarget";
	/// <summary>
	/// 플레이어 캐릭터를 나타내는 키 입니다.
	/// 적 캐릭터의 OnDamaged 메서드 호출 시 설정됩니다.
	/// </summary>
	public const string KEYNAME_PLAYERCHARACTER = "PlayerCharacter";
	/// <summary>
	/// 적의 생성 위치를 나타내는 키입니다.
	/// </summary>
	public const string KEYNAME_SPAWNLOCATION = "SpawnLocation";
	/// <summary>
	/// 생성 위치부터 이동 가능한 최대 거리를 나타냅니다.
	/// </summary>
	public const string KEYNAME_MAXMOVEDISTANCE = "MaxMoveDistance";
	/// <summary>
	/// 추적 최대 거리를 나타내는 키입니다.
	/// </summary>
	public const string KEYNAME_MAXTRACKINGDISTANCE = "MaxTrackingDistance";
	/// <summary>
	/// 공격 요청이 있음을 나타냅니다.
	/// </summary>
	public const string KEYNAME_ATTACKREQUESTED = "AttackRequested";
	/// <summary>
	/// 사망 상태를 나타냅니다.
	/// </summary>
	public const string KEYNAME_ISDEAD = "IsDead";


	protected virtual void Awake() 
	{
		AddKey(KEYNAME_ISAGGRESSIVESTATE, false);				// 공격적인 상태를 나타내는 키 설정
		AddKey(KEYNAME_LASTDAMAGEDTIME, 0.0f);					// 마지막으로 피해를 입은 시간
		AddKey(KEYNAME_TIMETOCALMDOWN, 0.0f);					// 공격적인 상태가 끝나기 까지 걸리는 시간
		AddKey(KEYNAME_MOVETARGET, Vector3.zero);				// 이동 목표 위치 키 설정
		AddKey(KEYNAME_PLAYERCHARACTER);						// 플레이어 캐릭터를 나타낼 키 설정
		AddKey(KEYNAME_SPAWNLOCATION, transform.position);		// 스폰 위치 설정
		AddKey(KEYNAME_MAXMOVEDISTANCE, 0.0f);					// 이동 반경 설정
		AddKey(KEYNAME_MAXTRACKINGDISTANCE, 3.0f);              // 추적 최대 거리 설정
		AddKey(KEYNAME_ATTACKREQUESTED, false);					// 공격 요청됨 여부
		AddKey(KEYNAME_ISDEAD, false);							// 사망 상태
	}

	protected virtual void Start() { }


#if UNITY_EDITOR
	protected override void OnDrawGizmosSelected()
	{
		base.OnDrawGizmosSelected();

		if (UnityEditor.EditorApplication.isPlaying)
		{
			// 이동 가능 범위 그리기
			Vector3 spawnPosition = GetKeyAsValue<Vector3>(KEYNAME_SPAWNLOCATION);
			float maxMoveDistance = GetKeyAsValue<float>(KEYNAME_MAXMOVEDISTANCE);
			float height = GetComponent<BoxCollider>().size.y;

			Gizmos.color = Color.black;
			Gizmos.DrawWireCube(spawnPosition, new Vector3(maxMoveDistance * 2, height));
		}
	}
#endif

}
