using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 등록된 행동들을 관리할 객체입니다.
/// 기본적으로 Task 와 Composite 를 이용하여 행동을 정의할 수 있습니다.
/// Task : 하나의 작업을 나타냅니다.
/// Composite : 분기가 실행되는 방식의 기본 규칙을 정의합니다.
/// Service : Task 와 Composite 에 추가하여 
/// 해당 분기가 실행중일 때 동시에 실행되며 특정한 데이터를 제공합니다.
/// </summary>
public class BehaviorController : MonoBehaviour
{
	/// <summary>
	/// 행동 객체에서 사용하게 될 데이터들을 가집니다.
	/// 외부에서 행동 객체에게 직접 접근하는 것이 어려우므로
	/// 사용되는 모든 데이터들은 BehaviorController 에서 소유하고 있도록 설계되었습니다.
	/// </summary>
	protected Dictionary<string, object> m_Keys = new();


	/// <summary>
	/// 행동 루틴을 나타내는 객체입니다.
	/// 오브젝트 파괴 시 루틴을 종료시키기 위해 사용됩니다.
	/// </summary>
	private Coroutine _BehaviorRoutine;

	/// <summary>
	/// 현재 실행중인 행동을 나타냅니다.
	/// </summary>
	private RunnableBehavior _CurrentBehavior;

	public void StartBehavior<TRunnableBehavior>()
		where TRunnableBehavior : RunnableBehavior, new()
	{
		_BehaviorRoutine = StartCoroutine(Run<TRunnableBehavior>());
	}

	private IEnumerator Run<TRunnableBehavior>()
		where TRunnableBehavior : RunnableBehavior, new()
	{
		// 등록된 행동들을 계속 실행시킵니다.
		while (true)
		{
			// 행동 객체를 생성
			TRunnableBehavior rootRunnable = new TRunnableBehavior();

			// 행동 객체 초기화
			rootRunnable.OnRunnableInitialized(this);

			// 변경된 행동 설정
			OnBehaviorChanged(rootRunnable);

			// 행동 실행
			yield return rootRunnable.RunBehavior();

			// 행동 종료
			rootRunnable.OnBehaviorFinished();

		}
	}

	/// <summary>
	/// 행동이 변경될 때 호출됩니다.
	/// </summary>
	/// <param name="newBehavior">바뀐 새로운 행동 객체가 전달됩니다.</param>
	protected virtual void OnBehaviorChanged(RunnableBehavior newBehavior)
	{
		_CurrentBehavior = newBehavior;
	}

	/// <summary>
	/// 행동 루틴을 종료합니다.
	/// </summary>
	public void StopBehavior()
	{
		// 현재 실행중인 행동 종료
		if (_CurrentBehavior != null)
		{
			_CurrentBehavior.OnBehaviorFinished();
			_CurrentBehavior = null;
		}

		// 행동 루틴 종료
		if (_BehaviorRoutine != null)
		{
			StopCoroutine(_BehaviorRoutine);
			_BehaviorRoutine = null;
		}
	}

	/// <summary>
	/// BehaviorController 객체에서 사용할 키를 추가합니다.
	/// </summary>
	/// <param name="keyName">사용할 키 이름을 전달합니다.</param>
	/// <param name="initialValue">키의 기본 값을 설정합니다.</param>
	public void AddKey(string keyName, object initialValue = null)
		=> m_Keys.Add(keyName, initialValue);

	/// <summary>
	/// BehaviorController 객체에서 사용하는 키의 값을 설정합니다.
	/// </summary>
	/// <param name="keyName">키 이름을 전달합니다.</param>
	/// <param name="value">설정시킬 값을 전달합니다.</param>
	public void SetKey(string keyName, object value)
		=> m_Keys[keyName] = value;

	/// <summary>
	/// 추가된 키의 값을 얻습니다.
	/// </summary>
	/// <param name="keyName">키 이름을 전달합니다.</param>
	/// <returns></returns>
	public object GetKey(string keyName)
	{
		if (m_Keys.TryGetValue(keyName, out object value))
		{
			return value;
		}

		return null;
	}

	/// <summary>
	/// 값 형태의 키값을 얻습니다.
	/// </summary>
	/// <typeparam name="T">반환받을 값 형식을 전달합니다.</typeparam>
	/// <param name="keyName">키 이름을 전달합니다.</param>
	/// <returns></returns>
	public T GetKeyAsValue<T>(string keyName)
		=> (T)GetKey(keyName);

	/// <summary>
	/// 참조 형태의 키값을 얻습니다.
	/// </summary>
	/// <typeparam name="T">반환받을 참조 형식을 전달합니다.</typeparam>
	/// <param name="keyName">키 이름을 전달합니다.</param>
	/// <returns></returns>
	public T GetKeyAsObject<T>(string keyName) where T : class
		=> GetKey(keyName) as T;


	protected virtual void OnDestroy()
	{
		// 행동 루틴 종료
		StopBehavior();
	}

#if UNITY_EDITOR
	protected virtual void OnDrawGizmosSelected()
	{
		string context = "";
		foreach(KeyValuePair<string, object> elem in m_Keys)
		{
			string keyName = elem.Key;
			object value = elem.Value;
			context += $"{keyName} [{value}]\n";
		}

		GUIStyle style = new();
		style.alignment = TextAnchor.UpperLeft;
		style.normal.textColor = Color.white;
		style.fontSize = 15;

		UnityEditor.Handles.Label(transform.position + Vector3.down, context, style);
	}
#endif

}

