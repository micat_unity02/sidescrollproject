using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 더미 캐릭터에서 사용되는 행동 컨트롤러 컴포넌트입니다.
/// </summary>
public sealed partial class BehaviorController_Dummy : BehaviorController_Enemy
{
	protected override void Awake()
	{
		base.Awake();

		SetKey(KEYNAME_TIMETOCALMDOWN, 3.0f);
	}

	protected override void Start()
	{
		base.Start();

		StartBehavior<RootSelector_Dummy>();
	}

}

public sealed partial class BehaviorController_Dummy : BehaviorController_Enemy
{
	// 루트에 등록될 행동 클래스를 정의합니다.
	public sealed class RootSelector_Dummy : BehaviorSelector
	{ 
		public RootSelector_Dummy()
		{
			// 대미지 대기중 상태
			AddBehavior<BT_WaitDamage>();

			// 공격 상태
			AddBehavior<Sequencer_AggressiveState>();
		}
	}

	public sealed class Sequencer_AggressiveState : BehaviorSequencer
	{
		public Sequencer_AggressiveState()
		{
			// 서비스 추가
			AddService<BSDummy_UpdateAggressiveState>();

			// 태스크 추가
			// 이동 목표 위치 설정
			//AddBehavior<BT_UpdatePlayerCharacterPosition>();
			AddBehavior(() => new BT_UpdatePlayerCharacterPosition(KEYNAME_MAXTRACKINGDISTANCE));
			AddBehavior(() => new BT_WaitMoveFinish(KEYNAME_MOVETARGET));
			AddBehavior(() => new BT_Wait(0.5f));
		}
	}
}

