﻿using UnityEngine;

public sealed class BSDummy_UpdateAggressiveState : BehaviorService
{
	public override void ServiceTick()
	{
		// 마지막으로 공격받은 시간을 얻습니다.
		float lastDamagedTime = behaviorController.GetKeyAsValue<float>(
			BehaviorController_Enemy.KEYNAME_LASTDAMAGEDTIME);

		// 공격적 상태가 풀리는 시간을 얻습니다.
		float timeToCalmDown = behaviorController.GetKeyAsValue<float>(
			BehaviorController_Enemy.KEYNAME_TIMETOCALMDOWN);

		// 게임 플레이 이후 지난 시간을 초단위로 얻습니다.
		float currentTimeSeconds = Time.time;

		if (currentTimeSeconds >= lastDamagedTime + timeToCalmDown)
		{
			behaviorController.SetKey(
				BehaviorController_Enemy.KEYNAME_ISAGGRESSIVESTATE, false);
		}
	}
}
