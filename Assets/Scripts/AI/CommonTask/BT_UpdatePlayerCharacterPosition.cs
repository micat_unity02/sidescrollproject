﻿using System.Collections;
using UnityEngine;

public class BT_UpdatePlayerCharacterPosition : RunnableBehavior
{
	/// <summary>
	/// 최대 거리를 나타내는 키
	/// 만약 캐릭터가 멀더라도, 이 키의 값만큼 거리를 조절합니다.
	/// </summary>
	private readonly string KEYNAME_MAXDISTANCE;

	public BT_UpdatePlayerCharacterPosition(string maxDistanceKey)
	{
		KEYNAME_MAXDISTANCE = maxDistanceKey;
	}

	public override IEnumerator RunBehavior()
	{
		// 플레이어 캐릭터 객체를 얻습니다.
		PlayerCharacter playerCharacter = behaviorController.GetKeyAsObject<PlayerCharacter>(
			BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER);
		if (playerCharacter == null)
		{
			isSucceeded = false;
			yield break;
		}

		// 플레이어 캐릭터의 위치를 얻습니다.
		Vector3 playerCharacterPosition = playerCharacter.transform.position;

		// 현재 위치를 얻습니다.
		Vector3 currentPosition = behaviorController.transform.position;

		// 최대 거리
		float maxDistance = behaviorController.GetKeyAsValue<float>(KEYNAME_MAXDISTANCE);

		float distanceX = playerCharacterPosition.x - currentPosition.x;

		// 최대 거리 설정
		if (Mathf.Abs(distanceX) > maxDistance)
		{
			distanceX = maxDistance * Mathf.Sign(distanceX);
		}
		Vector3 targetPosition = currentPosition + (Vector3.right * distanceX);

		// 목표 위치를 설정합니다.
		behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_MOVETARGET,
			targetPosition);

		isSucceeded = true;
	}

}
