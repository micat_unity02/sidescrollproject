﻿using System.Collections;
using UnityEngine;

public class BT_WaitDamage : RunnableBehavior
{
	private bool _IsAggressiveState;

	public override IEnumerator RunBehavior()
	{
		// 공격적인 상태인지 확인합니다.
		_IsAggressiveState = IsAggressiveState();

		isSucceeded = !_IsAggressiveState;

		yield return null;
	}

	private bool IsAggressiveState() => behaviorController.GetKeyAsValue<bool>(
		BehaviorController_Enemy.KEYNAME_ISAGGRESSIVESTATE);
}
