﻿using System.Collections;
using UnityEngine;


public class BT_WaitMoveFinish : RunnableBehavior
{
	/// <summary>
	/// 목표 위치를 나타내는 키
	/// </summary>
	private string _MoveTargetKey;

	public BT_WaitMoveFinish(string moveTargetKey)
	{
		_MoveTargetKey = moveTargetKey;
	}

	public override IEnumerator RunBehavior()
	{
		// 이동을 끝낼때까지 대기합니다.
		yield return new WaitUntil(IsMoveFinish);
		isSucceeded = true;
	}

	private bool IsMoveFinish()
	{
		Vector3 targetPosition = behaviorController.GetKeyAsValue<Vector3>(_MoveTargetKey);
		return behaviorController.transform.position == targetPosition;
	}
}