﻿using System.Collections;
using UnityEngine;

public class BT_GetRandomPosition : RunnableBehavior
{
	public readonly string KEYNAME_SPAWNPOSITION;
	public readonly string KEYNAME_MAXMOVEDISTANCE;
	public readonly string KEYNAME_TARGETPOSITION;

	public BT_GetRandomPosition(
		string spawnPositionKey,
		string maxMoveDistanceKey,
		string targetPositionKey)
	{
		KEYNAME_SPAWNPOSITION = spawnPositionKey;
		KEYNAME_MAXMOVEDISTANCE = maxMoveDistanceKey;
		KEYNAME_TARGETPOSITION = targetPositionKey;
	}

	public override IEnumerator RunBehavior()
	{
		// 스폰 위치
		Vector3 spawnPosition = behaviorController.GetKeyAsValue<Vector3>(
			KEYNAME_SPAWNPOSITION);

		// 최대 이동 가능 거리
		float maxMoveDistance = behaviorController.GetKeyAsValue<float>(
			KEYNAME_MAXMOVEDISTANCE);

		// 랜덤 위치 X
		float randomPositionX = Random.Range(
			spawnPosition.x - maxMoveDistance, 
			spawnPosition.x + maxMoveDistance);

		// 랜덤 위치를 설정합니다.
		behaviorController.SetKey(KEYNAME_TARGETPOSITION,
			new Vector3(randomPositionX, behaviorController.transform.position.y));

		isSucceeded = true;
		yield return null;
	}
}