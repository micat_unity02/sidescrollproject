using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed partial class BehaviorController_Enemy01 : BehaviorController_Enemy
{
	/// <summary>
	/// 마지막으로 공격한 시간
	/// </summary>
	public const string KEYNAME_LASTATTACKTIME = "LastAttackTime";
	/// <summary>
	/// 공격 시간 텀
	/// </summary>
	public const string KEYNAME_ATTACKTERM = "AttackTerm";
	/// <summary>
	/// 이동을 블록시킵니다.
	/// </summary>
	public const string KEYNAME_BLOCKMOVEMENT = "BlockMovement";


	protected override void Awake()
	{
		base.Awake();

		AddKey(KEYNAME_LASTATTACKTIME, Time.time - 4.0f);
		AddKey(KEYNAME_ATTACKTERM, 4.0f);
		AddKey(KEYNAME_BLOCKMOVEMENT, false);

		SetKey(KEYNAME_MAXMOVEDISTANCE, 5.0f);
	}

	protected override void Start()
	{
		base.Start();

		StartEnemy01Behavior();
	}

	public void StartEnemy01Behavior()
	{
		StartBehavior<RootSelector_Enemy01>();
	}
}

public sealed partial class BehaviorController_Enemy01 : BehaviorController_Enemy
{
	public class RootSelector_Enemy01 : BehaviorSelector
	{
		public RootSelector_Enemy01()
		{
			// 살아있는 상태
			AddBehavior<AliveStateSelector_Enemy01>();

			// 사망 상태
			AddBehavior(() => new BT_Wait(2.0f));
		}
	}

	public class AliveStateSelector_Enemy01 : BehaviorSelector
	{
		public AliveStateSelector_Enemy01()
		{
			// 대기 상태
			AddBehavior<WaitDamageSequencer>();

			// 공격 상태
			AddBehavior<AggressiveSelector>();
		}

		public override IEnumerator RunBehavior()
		{
			bool isDead = behaviorController.GetKeyAsValue<bool>(KEYNAME_ISDEAD);

			if (isDead)
			{
				behaviorController.SetKey(KEYNAME_MOVETARGET, behaviorController.transform.position);

				isSucceeded = false;
				yield break;
			}

			yield return base.RunBehavior();
		}

		public class WaitDamageSequencer : BehaviorSequencer
		{ 
			public WaitDamageSequencer()
			{
				// 1.5 초 대기
				AddBehavior(() => new BT_Wait(1.5f));

				// 랜덤한 위치로 이동하기 위해 목표 위치를 설정합니다.
				AddBehavior(() => new BT_GetRandomPosition(
					KEYNAME_SPAWNLOCATION,
					KEYNAME_MAXMOVEDISTANCE,
					KEYNAME_MOVETARGET));

				// 이동이 끝날 때까지 대기합니다.
				AddBehavior(() => new BT_WaitMoveFinish(KEYNAME_MOVETARGET));
			}


			public override IEnumerator RunBehavior()
			{
				// 공격적 상태인지 확인합니다.
				bool isAggressiveState = behaviorController.GetKeyAsValue<bool>(
					KEYNAME_ISAGGRESSIVESTATE);

				// 만약 공격적 상태라면 행동 실행 실패!
				if (isAggressiveState)
				{
					isSucceeded = false;
					yield break;
				}

				// 공격적 상태가 아니라면, 주어진 행동을 실행합니다.
				yield return base.RunBehavior();
			}
		}
		public class AggressiveSelector : BehaviorSelector
		{
			public AggressiveSelector()
			{
				// 플레이어 추적
				AddBehavior<TrackingSelector>();

				// 공격 실행
				AddBehavior<BT_Enemy01Attack>();
			}

			public class TrackingSelector : BehaviorSelector
			{
				public TrackingSelector()
				{
					// 공격 영역 검사 서비스 추가
					AddService(() => new BS_CheckAttackArea(
						KEYNAME_ATTACKREQUESTED,
						KEYNAME_LASTATTACKTIME,
						KEYNAME_ATTACKTERM,
						behaviorController.transform.position,
						new Vector3(3.0f, 1.0f, 1.0f),
						1 << LayerMask.NameToLayer(Constants.LAYERNAME_PLAYER)));



					// 플레이어 캐릭터 위치를 목표 위치로 설정합니다.
					AddBehavior(() => new BT_UpdatePlayerCharacterPosition(
						KEYNAME_MAXTRACKINGDISTANCE));

					// 이동을 대기합니다.
					AddBehavior(() => new BT_WaitMoveFinish(KEYNAME_MOVETARGET));
				}

				public override IEnumerator RunBehavior()
				{
					// 공격 요청을 받은 경우 실행 실패
					bool isAttackRequested = behaviorController.GetKeyAsValue<bool>(
						KEYNAME_ATTACKREQUESTED);

					if (isAttackRequested)
					{
						isSucceeded = false;
						yield break;
					}

					yield return base.RunBehavior();
				}

			}
		}
	}



}
