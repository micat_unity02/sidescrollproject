﻿using System.Collections;
using UnityEngine;

/// <summary>
/// 등록된 행동들을 순차적으로 실행하는 객체입니다.
/// 등록된 순서대로 행동을 실행하며, 실행한 행동이 성공한다면 분기를 종료시킵니다.
/// </summary>
public class BehaviorSelector : BehaviorCompositeBase
{

	public BehaviorSelector() { }
	public BehaviorSelector(params System.Func<RunnableBehavior>[] runnables) : base(runnables) { }

	public override IEnumerator RunBehavior()
	{
		// 기본적으로 성공 상태에서 실행되도록 합니다.
		isSucceeded = false;

		foreach (System.Func<RunnableBehavior> getRunnable in m_Runnables)
		{
			// 등록한 행동 객체를 생성합니다.
			RunnableBehavior runnable = getRunnable.Invoke();

			// 행동 객체 초기화
			runnable.OnRunnableInitialized(behaviorController);

			// 행동 실행
			yield return runnable.RunBehavior();

			// 행동 끝
			runnable.OnBehaviorFinished();

			// 실행한 행동이 성공한 경우 다음 행동을 실행하지 않도록 합니다.
			if (runnable.isSucceeded)
			{
				// 다음 행동을 실행하지 않도록 합니다.
				isSucceeded = true;
				yield break;
			}
		}
	}
}
