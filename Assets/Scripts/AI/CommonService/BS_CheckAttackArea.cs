using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BS_CheckAttackArea : BehaviorService
{
	// 플레이어 감지 시 공격 요청을 위한 키 이름입니다.
	private readonly string KEYNAME_ATTATTACKREQUEST;

	// 마지막으로 공격한 시간을 저장하는 키 이름
	private readonly string KEYNAME_LASTATTACKTIME;

	// 공격 텀 키 이름
	private readonly string KEYNAME_ATTACKTERM;

	/// <summary>
	/// 검사 영역의 중앙 위치입니다.
	/// </summary>
	private readonly Vector3 _AreaCenter;

	/// <summary>
	/// 검사 영역의 크기입니다.
	/// </summary>
	private readonly Vector3 _AreaSize;

	/// <summary>
	/// 검사시킬 플레이어 캐릭터의 레이어를 전달합니다.
	/// </summary>
	private LayerMask _PlayerCharacterLayer;


	public BS_CheckAttackArea(
		string attackRequestKey, 
		string lastAttackTimeKey,
		string attackTermKey,
		Vector3 areaCenter, 
		Vector3 areaSize,
		LayerMask playerCharacterLayer)
	{
		KEYNAME_ATTATTACKREQUEST = attackRequestKey;
		KEYNAME_LASTATTACKTIME = lastAttackTimeKey;
		KEYNAME_ATTACKTERM = attackTermKey;

		_AreaCenter = areaCenter;
		_AreaSize = areaSize;
		_PlayerCharacterLayer = playerCharacterLayer;
	}


	public override void ServiceTick()
	{
		// 마지막으로 공격한 시간
		float lastAttackTime = behaviorController.GetKeyAsValue<float>(
			KEYNAME_LASTATTACKTIME);

		// 공격 시간 텀
		float attackTerm = behaviorController.GetKeyAsValue<float>(
			KEYNAME_ATTACKTERM);

		if (Time.time >= lastAttackTime + attackTerm)
		{
			CheckArea();
		}
	}

	private void CheckArea()
	{
		// 영역을 검사합니다.
		Collider[] detectedCollisions = Physics.OverlapBox(
			_AreaCenter, _AreaSize * 0.5f, Quaternion.identity, _PlayerCharacterLayer);

		// 감지 결과가 존재하지 않는다면 실행 취소
		if (detectedCollisions.Length == 0) return;

		// 감지 결과가 존재한다면, 공격 요청
		behaviorController.SetKey(KEYNAME_ATTATTACKREQUEST, true);

	}
}
