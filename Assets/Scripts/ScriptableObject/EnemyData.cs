﻿using System;
using UnityEngine;

[System.Serializable]
public class EnemyDataElem
{
	public string m_EnemyCode;
	public EnemyData m_EnemyData;
}

/// <summary>
/// 적 하나의 정보를 나타냅니다.
/// </summary>
[System.Serializable]
public struct EnemyData
{
	[Header("적 이름")]
	public string enemyName;

	[Header("적 최대 체력")]
	public float maxHP;

	[Header("공격력")]
	public float atk;

	[Header("방어력")]
	public float def;
}