﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MapSectionData", menuName = "ScriptableObject/MapSectionData")]
public class MapSectionScriptableObject : ScriptableObject
{
	public List<MapSectionDataElem> m_MapSectionDatas;

	public MapSectionDataElem GetMapSectionData(int id)
		=> m_MapSectionDatas.Find((sectionDataElem) => sectionDataElem.mapSectionID == id);
}
