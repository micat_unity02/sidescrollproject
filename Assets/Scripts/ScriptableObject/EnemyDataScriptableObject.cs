﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "ScriptableObject/EnemyData")]
public class EnemyDataScriptableObject : ScriptableObject
{
	/// <summary>
	/// 적 정보를 나타냅니다.
	/// </summary>
	public List<EnemyDataElem> m_EnemyData;

	/// <summary>
	/// 적 정보를 얻습니다.
	/// </summary>
	/// <param name="enemyCode">찾을 적 코드를 전달합니다.</param>
	/// <returns>enemyCode 와 일치하는 적 정보를 반환합니다.</returns>
	public EnemyData GetEnemyData(string enemyCode)
	{
		// enemyCode 와 일치하는 적 정보를 찾습니다.
		EnemyDataElem enemyDataElem = m_EnemyData.Find(
			(EnemyDataElem elem) => elem.m_EnemyCode == enemyCode);

		// 만약 enemyCode 와 일치하는 적 정보를 찾지 못한 경우 예외 발생.
		if (enemyDataElem == null)
		{
			Debug.LogError($"적 정보를 찾을 수 없습니다. (CODE : {enemyCode})");
			throw new System.Exception();
		}

		// 적 정보를 반환합니다.
		return enemyDataElem.m_EnemyData;
	}

}
